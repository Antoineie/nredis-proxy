/**
 * 
 */
package com.opensource.netty.redis.proxy.spring.schema.support;

import java.io.Serializable;
import java.util.List;

import com.opensource.netty.redis.proxy.core.cluster.LoadBalance;
import com.opensource.netty.redis.proxy.core.config.RedisPoolConfig;
/**
 * 主节点
 * @author liubing
 *
 */
public class RedisProxyMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6861660674247475024L;
	
	private String host;//主机名
	
	private int port;//端口号
	
	private RedisPoolConfig redisPoolConfig;//连接池配置
	
	private List<RedisProxySlave> redisProxyClusters;//多个从
	
	private LoadBalance loadClusterBalance;//从权重
	
	/**
	 * 
	 */
	public RedisProxyMaster() {
		super();
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the redisProxyClusters
	 */
	public List<RedisProxySlave> getRedisProxyClusters() {
		return redisProxyClusters;
	}

	/**
	 * @param redisProxyClusters the redisProxyClusters to set
	 */
	public void setRedisProxyClusters(List<RedisProxySlave> redisProxyClusters) {
		this.redisProxyClusters = redisProxyClusters;
	}
	
	/**
	 * @return the key
	 */
	public String getKey() {
		StringBuilder stringBuilder=new StringBuilder();
		stringBuilder.append(host).append("-").append(port);
		return stringBuilder.toString();
	}

	/**
	 * @return the loadClusterBalance
	 */
	public LoadBalance getLoadClusterBalance() {
		return loadClusterBalance;
	}

	/**
	 * @param loadClusterBalance the loadClusterBalance to set
	 */
	public void setLoadClusterBalance(LoadBalance loadClusterBalance) {
		this.loadClusterBalance = loadClusterBalance;
	}

	/**
	 * @return the redisPoolConfig
	 */
	public RedisPoolConfig getRedisPoolConfig() {
		return redisPoolConfig;
	}

	/**
	 * @param redisPoolConfig the redisPoolConfig to set
	 */
	public void setRedisPoolConfig(RedisPoolConfig redisPoolConfig) {
		this.redisPoolConfig = redisPoolConfig;
	}

	
}
